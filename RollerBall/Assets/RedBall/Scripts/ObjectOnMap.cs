﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectOnMap : MonoBehaviour
{
    private GameObject Target;
    private bool AutoPush;
    
    public void DestroyObject()
    {

    }

    public void Push(float Velocity)
    {
        if (gameObject.CompareTag("Crate"))
        {
            if(!AutoPush)
            {
                AutoPush = true;
                StopCoroutine("AutoPushCrate");
                StartCoroutine("AutoPushCrate", Velocity);
            }
        }
    }

    IEnumerator CountToDestroy()
    {
        yield return new WaitForSeconds(5);
        Destroy(gameObject);
    }

    IEnumerator AutoPushCrate(float _Velocity)
    {
        while (true)
        {
            //transform.GetComponent<Rigidbody2D>().AddForce(new Vector2(-_Velocity * 20, transform.GetComponent<Rigidbody2D>().velocity.y));
            transform.GetComponent<Rigidbody2D>().velocity = new Vector2(-_Velocity, transform.GetComponent<Rigidbody2D>().velocity.y) ;//(new Vector2(-_Velocity * 20, transform.GetComponent<Rigidbody2D>().velocity.y));
            yield return null;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            AutoPush = false;
            Target = null;
            StopCoroutine("AutoPushCrate");
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("DeadZone"))
        { 
            gameObject.SetActive(false);
        }
    }
}
