﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using DragonBones;

public class Enermy : MonoBehaviour
{
    [Header("Custom-------------------------")]
    public MoveType MovingStyle;
    public ActionType ActionStyle;
    public float MoveRange;
    public float ForwardSpeed;
    public float BackwardSpeed;
    public bool Invincible;
    [Header("Action-------------------------")]
    public float ReactionTime;
    public float BuletSpeed;
    public float LaughtStrenght;
    public float LaughtHigh;
    public int MaxBulletOnMap;

    [Header("Control------------------------")]
    [SerializeField] private UnityArmatureComponent Anim; 
    [SerializeField] private Collider2D Collider;
    [SerializeField] private Rigidbody2D Rb;
    [SerializeField] private GameObject Spy;
    [SerializeField] private UnityEngine.Transform PointShot;
    [SerializeField] private UnityEngine.Transform AmmoPool;
    [SerializeField] private GameObject Bullet;
    [SerializeField] private GameObject Bomb;
    private bool IsDead;

    private void Start()
    {
        CheckMoveType(MovingStyle);
        CheckActionType(ActionStyle);
    }

    private void CheckMoveType(MoveType _MovingStyle)
    {
        //transform.DOKill();
        switch (_MovingStyle)
        {
            case MoveType.STAND:
                {
                    Anim.animation.Play("Idle");
                    break;
                }
            case MoveType.PATROL:
                {
                    Anim.animation.Play("walk");
                    ValueMovement Movement = new ValueMovement();
                    Movement.TimeTweenForward = (MoveRange * 1.5f) / ForwardSpeed;
                    Movement.TimeTweenBackward = (MoveRange * 1.5f) / BackwardSpeed;
                    Movement.StartPos = transform.position.x - ((MoveRange * 1.5f) / 2f);
                    Movement.EndPos = transform.position.x + ((MoveRange * 1.5f) / 2f);
                    StopCoroutine("MoveLeftRight");
                    StartCoroutine("MoveLeftRight", Movement);
                    break;
                }
            case MoveType.RIGHT_BACK:
                {
                    Anim.animation.Play("walk");
                    ValueMovement Movement = new ValueMovement();
                    Movement.TimeTweenForward = (MoveRange * 1.5f) / ForwardSpeed;
                    Movement.TimeTweenBackward = (MoveRange * 1.5f) / BackwardSpeed;
                    Movement.StartPos = transform.position.x;
                    Movement.EndPos = transform.position.x + MoveRange * 1.5f;
                    StopCoroutine("MoveLeftRight");
                    StartCoroutine("MoveLeftRight", Movement);
                    break;
                }
            case MoveType.LEFT_BACK:
                {
                    Anim.animation.Play("walk");
                    ValueMovement Movement = new ValueMovement();
                    Movement.TimeTweenForward = (MoveRange * 1.5f) / ForwardSpeed;
                    Movement.TimeTweenBackward = (MoveRange * 1.5f) / BackwardSpeed;
                    Movement.StartPos = transform.position.x;
                    Movement.EndPos = transform.position.x - MoveRange * 1.5f;
                    StopCoroutine("MoveLeftRight");
                    StartCoroutine("MoveLeftRight", Movement);
                    break;
                }
            case MoveType.UP_DOWN:
                {
                    ValueMovement Movement = new ValueMovement();
                    Movement.TimeTweenForward = (MoveRange * 1.5f) / ForwardSpeed;
                    Movement.TimeTweenBackward = (MoveRange * 1.5f) / BackwardSpeed;
                    Movement.StartPos = transform.position.y;
                    Movement.EndPos = transform.position.y - MoveRange * 1.5f;
                    StopCoroutine("MoveUpDown");
                    StartCoroutine("MoveUpDown", Movement);
                    break;
                }
        }
    }

    private void CheckActionType(ActionType _ActionStyle)
    {
        switch (_ActionStyle)
        {
            case ActionType.NONE:
                {
                    break;
                }
            case ActionType.SHOT_LEFT:
                {
                    StartCoroutine("Shot", Vector2.left);
                    break;
                }
            case ActionType.SHOT_RIGHT:
                {
                    StartCoroutine("Shot", Vector2.right);
                    break;
                }
            case ActionType.SHOT_UP:
                {
                    StartCoroutine("Shot", Vector2.up);
                    break;
                }
            case ActionType.SHOT_DOWN:
                {
                    StartCoroutine("Shot", Vector2.down);
                    break;
                }
            case ActionType.LAUNCH_LEFT:
                {
                    break;
                }
            case ActionType.LAUNCH_RIGHT:
                {
                    break;
                }
        }
    }

    private IEnumerator Shot(Vector2 _VectorShot)
    {
        if (MaxBulletOnMap > 0)
        {
            for (int index = 0; index < MaxBulletOnMap; index++)
            {
                GameObject _Bullet= (GameObject) Instantiate(Bullet, AmmoPool);
                _Bullet.SetActive(false);
            }
            Anim.animation.timeScale = ReactionTime / 3.9f;

            while (true)
            {
                yield return new WaitForSeconds(ReactionTime / 2);
                for (int index = 0; index < AmmoPool.childCount; index++)
                {
                    if (!AmmoPool.GetChild(index).gameObject.activeInHierarchy)
                    {
                        Anim.animation.Play("Shoot");
                        yield return new WaitForSeconds(ReactionTime / 2);
                        GameObject _Bullet;
                        _Bullet = AmmoPool.GetChild(index).gameObject;
                        _Bullet.SetActive(true);
                        _Bullet.transform.position = PointShot.position;
                        _Bullet.GetComponent<BulletMonster>().SetSpeed(_VectorShot, BuletSpeed);
                        break;
                    }
                    else
                    if (index == AmmoPool.childCount - 1)
                    {
                        Anim.animation.Stop("Idle");
                        Anim.animation.Play("Idle");
                    }
                }
            }
        }
    }

    private IEnumerator MoveLeftRight(ValueMovement _MovementValue)
    {
        yield return new WaitForSeconds(0.3f);
        while (true)
        {
            transform.DOMove(new Vector3(_MovementValue.EndPos, transform.position.y), _MovementValue.TimeTweenForward).SetEase(Ease.Flash);
            yield return new WaitForSeconds(_MovementValue.TimeTweenForward);
            //transform.DOKill();
            transform.DOMove(new Vector3(_MovementValue.StartPos, transform.position.y), _MovementValue.TimeTweenBackward).SetEase(Ease.Flash);
            yield return new WaitForSeconds(_MovementValue.TimeTweenBackward);
        }
    }

    private IEnumerator MoveUpDown(ValueMovement _MovementValue)
    {
        while (true)
        {
            transform.DOMove(new Vector3(transform.position.x, _MovementValue.EndPos), _MovementValue.TimeTweenForward).SetEase(Ease.Flash);
            Anim.animation.Play("Idle");
            yield return new WaitForSeconds(_MovementValue.TimeTweenForward);
            transform.DOKill();
            Anim.animation.Play("Climb");
            transform.DOMove(new Vector3(transform.position.x, _MovementValue.StartPos), _MovementValue.TimeTweenBackward).SetEase(Ease.Flash);
            yield return new WaitForSeconds(_MovementValue.TimeTweenBackward);
        }
    }

    public void Dead()
    {
        StopCoroutine("Shot");
        StopCoroutine("MoveLeftRight");
        StopCoroutine("MoveUpDown");
        Anim.animation.Play("Idle");
        transform.DOKill();
        Collider.enabled = false;
        Vector3 ShockStreght = new Vector3(0, 200, 0);
        Rb.constraints = RigidbodyConstraints2D.None;
        Rb.AddForce(ShockStreght);
        Rb.gravityScale = 3;
        StopCoroutine("TimeOffObj");
        StartCoroutine("TimeOffObj");
    }

    private IEnumerator TimeOffObj()
    {
        yield return new WaitForSeconds(1);
        gameObject.SetActive(false);
        if (Spy)
        {
            Spy.SetActive(false);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("DeadZone"))
        { 
            gameObject.SetActive(false);
        }
    }
}

public class ValueMovement
{
    public float TimeTweenForward;
    public float TimeTweenBackward;
    public float StartPos;
    public float EndPos;
}
