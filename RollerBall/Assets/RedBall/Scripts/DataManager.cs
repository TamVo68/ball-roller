﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class DataManager : MonoBehaviour
{
    public Self PlayeData = new Self();
    public Config ConfigData;

    private void Awake()
    {
        ConstantManager.Data = this;
    }

    public void SaveData()
    {
        Self dt = new Self();
        dt.CurrentStage = ConstantManager.Data.PlayeData.CurrentStage;

        string JSON = JsonUtility.ToJson(dt);
        string Path = Application.persistentDataPath + "/" + "User" + ".dat";
        File.WriteAllText(Path, JSON);
        Debug.Log("Save Ok");
    }

    public void LoadData()
    {
        if (File.Exists(Application.persistentDataPath + "/" + "User" + ".dat"))
        {
            Self dt = new Self();
            string Path = Application.persistentDataPath + "/" + "User" + ".dat";
            string JSON = File.ReadAllText(Path);
            dt = JsonUtility.FromJson<Self>(JSON);

            ConstantManager.Data.PlayeData.CurrentStage = dt.CurrentStage;
            Debug.Log("Load Ok");
        }
        else
        {
            SaveData();
            Debug.Log("File save not exist -> Save Data");
        }
    }
}
