﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Tilemaps;
using TMPro;

public class RollerBallManager : MonoBehaviour
{
    #region Manager
    [Header("Manager--------------------")]
    [SerializeField] private List<GameObject> Stages;

    public bool CheckAlStarClear()
    {
        if (TakeStars >= MaxStars)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    #endregion

    #region Flow Panel
    [Header(">Main Menu----------")]
    [SerializeField] private GameObject MainPanel;
    [SerializeField] private GameObject MainMenuPanel;
    [Header(">Stage Select-------")]
    [SerializeField] private GameObject StageSelectPanel;
    [SerializeField] private Transform StageContent;
    [SerializeField] private GameObject SelectStageOBJ;
    [SerializeField] private Transform LoadStageContent;
    [SerializeField] private TextMeshProUGUI[] CurrentCoin;
    [SerializeField] private TextMeshProUGUI CurrentKey;
    #endregion

    #region Control
    [Header("GamePlay-------------------")]
    [SerializeField] private CharacterCTRL Player;
    [SerializeField] private MoveCamera PlayerCamera;
    [SerializeField] private GameObject PlayPanel;
    [Header("Movement-------------------")] 
    [SerializeField] private Button MoveLBTN;
    [SerializeField] private Button MoveRBTN;
    [SerializeField] private Button JumpBTN;
    [Header("UI-------------------------")]
    [SerializeField] private Button StartBTN;
    [SerializeField] private Button[] BackToMainMenuBTN;
    [SerializeField] private Button[] RestartBTN;
    [SerializeField] private Button[] BackToSelectBTN;
    [SerializeField] private Button[] PlayNextBTN;
    [SerializeField] private Button[] PlayADSBTN;
    [Header(">Playing------------")]
    [SerializeField] private Button PauseBTN;
    [Header(">Player-------------")]
    [SerializeField] private Image StarsLine;
    [SerializeField] private TextMeshProUGUI StarsCount;
    [SerializeField] private GameObject[] Hearts;
    #endregion

    #region Game Play
    [Header(">Stage Select------")]
    [SerializeField] private int CurrentPage;
    [Header(">Stage-------------")]
    private Transform CheckPoint;
    private TilemapRenderer MapSize;
    private int TimeStage_s;
    private int TakeStars;
    private int MaxStars;
    private int StageIDIsLoad;
    [Header(">Player------------")]
    public int HP;
    public bool CanHurt;
    public int KeyValue;
    #endregion

    # region Win Lose
    [Header("Win Lose-------------------")]
    [SerializeField] private GameObject WinLosePanel;
    [SerializeField] private GameObject WinPanel;
    [SerializeField] private GameObject LosePanel;
    #endregion

    private void Awake()
    {
        ConstantManager.Game_Manager = this;
        EventManager.Event_WinLose += WinLose;
        EventManager.Event_StartGame += StartGame;
        EventManager.Event_UpdateTakeStar += UpdateTakeStar;
        EventManager.Event_UpdateCoin += UpdateCoin;
        EventManager.Event_TakeStar += TakeStar;
        EventManager.Event_TakeCoin += TakeCoin;
        EventManager.Event_SubHP += SubHP;
        EventManager.Event_SelectStage += SelectStage;
        EventManager.Event_TakeKey += TakeKey;
        EventManager.Event_UpdateKey += UpdateKey;
    }

    private void OnDisable()
    {
        EventManager.Event_WinLose -= WinLose;
        EventManager.Event_StartGame -= StartGame;
        EventManager.Event_UpdateTakeStar -= UpdateTakeStar;
        EventManager.Event_UpdateCoin -= UpdateCoin;
        EventManager.Event_TakeStar -= TakeStar;
        EventManager.Event_TakeCoin -= TakeCoin;
        EventManager.Event_SubHP -= SubHP;
        EventManager.Event_SelectStage -= SelectStage;
        EventManager.Event_TakeKey -= TakeKey;
        EventManager.Event_UpdateKey -= UpdateKey;
    }

    //Start is called before the first frame update
    private IEnumerator Start()
    {
        OpenGame();
        Player.GetComponent<MoveControl>().SetUpControl(MoveLBTN, MoveRBTN, JumpBTN, Player.gameObject);
        yield return new WaitForSeconds(0.5f);
    }

    private void OpenGame()
    {
        ConstantManager.Data.LoadData();
        PlayerCamera.gameObject.SetActive(false);
        Player.gameObject.SetActive(false);
        StageSelectPanel.gameObject.SetActive(false);
        PlayPanel.SetActive(false);
        MainMenuPanel.SetActive(true);
        EventManager.Event_UpdateCoin();
        StartBTN.onClick.AddListener(() => 
        {
            MainMenuPanel.SetActive(false);
            StageSelectPanel.SetActive(true);
            OpenStageSelect();
        });

        foreach (Button BTN in RestartBTN)
        {
            BTN.onClick.AddListener(()=> 
            {
                EventManager.Event_SelectStage(StageIDIsLoad);
            });
        }

        foreach (Button BTN in BackToSelectBTN)
        {
            BTN.onClick.AddListener(() =>
            {
                BackToSelect();
            });
        }

        foreach (Button BTN in PlayNextBTN)
        {
            BTN.onClick.AddListener(() =>
            {
                StageIDIsLoad++;
                if (StageIDIsLoad > ConstantManager.Data.PlayeData.CurrentStage)
                { 
                    StageIDIsLoad = ConstantManager.Data.PlayeData.CurrentStage;
                }
                EventManager.Event_SelectStage(StageIDIsLoad);
            });
        }

        foreach (Button BTN in BackToMainMenuBTN)
        {
            BTN.onClick.AddListener(() =>
            {
                BackToMainMenu();
            });
        }
    }

    private void BackToMainMenu()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private void BackToSelect()
    {
        PlayerCamera.gameObject.SetActive(false);
        Player.gameObject.SetActive(false);
        LoadStageContent.gameObject.SetActive(false);
        WinLosePanel.SetActive(false);
        PlayPanel.SetActive(false);
        CurrentPage = 0;
        OpenStageSelect();
    }

    private void OpenStageSelect()
    {
        MainPanel.SetActive(true);
        StageContent.GetComponent<GridLayoutGroup>().enabled = false;
        if (StageContent.childCount > 0)
        {
            for (int index = 0; index < StageContent.childCount; index++)
            {
                Destroy(StageContent.GetChild(index).gameObject);
            }
        }

        if (Stages.Count > 0)
        {
            for (int index = 0; index < Stages.Count; index++)
            {
                GameObject SelectBTN = (GameObject)Instantiate(SelectStageOBJ, StageContent);
                SelectBTN.GetComponent<StageSelectCTRL>().InputInfo((CurrentPage) * 20 + index);
            }
        }
        StageContent.GetComponent<GridLayoutGroup>().enabled = true;
    }

    private void StartGame(Transform _CheckPoint, TilemapRenderer _MapSize, int _Stars, int _TimeStage_s, Transform Start)
    {
        PlayerCamera.gameObject.SetActive(false);
        Player.gameObject.SetActive(false);
        PlayerCamera.SetControl(Player.gameObject, _MapSize);
        Player.GetComponent<MoveControl>().SetStartPoint(_CheckPoint);
        PlayerCamera.gameObject.SetActive(true);
        PlayerCamera.StartFollow();
        Player.gameObject.SetActive(true);
        Player.SetStartPoint(Start);
        Player.Respawn();
        foreach (var Heart in Hearts)
        {
            Heart.SetActive(true);
        }
        HP = 3;
        KeyValue = 0;
        TimeStage_s = _TimeStage_s;
        MaxStars = _Stars;
        TakeStars = 0;
        CanHurt = true;
        EventManager.Event_UpdateTakeStar();
    }

    private void StopGame()
    {
        PlayerCamera.gameObject.SetActive(false);
        Player.gameObject.SetActive(false);
        StageContent.gameObject.SetActive(false);
        PlayPanel.gameObject.SetActive(false);
    }

    private void WinLose(bool IsWin)
    {
        WinLosePanel.SetActive(false);
        WinPanel.SetActive(false);
        LosePanel.SetActive(false);
        WinLosePanel.SetActive(true);
        if (IsWin)
        {
            if (TakeStars == MaxStars)
            {
                ConstantManager.Data.PlayeData.CurrentStage++;
                ConstantManager.Data.SaveData();
                WinPanel.SetActive(true);
                if (StageIDIsLoad >= Stages.Count - 1)
                {
                    foreach (var BTN in PlayNextBTN)
                    {
                        BTN.interactable = false;
                    }
                }
                else
                {
                    foreach (var BTN in PlayNextBTN)
                    {
                        BTN.interactable = true;
                    }
                }
            }
        }
        else
        { 
            LosePanel.SetActive(true);
        }
        //StartCoroutine("CountdownRestartMap", 3);
    }

    IEnumerator CountdownRestartMap(float TimeWait)
    {
        yield return new WaitForSeconds(TimeWait);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private void SubHP(int _DMG)
    {
        if (CanHurt)
        {
            HP -= _DMG;
            UpdateHP();
            CanHurt = false;
            if (HP <= 0)
            {
                Player.PlayerAvata.SetActive(false);
                Player.State = PlayerState.DEAD;
                Player.GetComponent<Rigidbody2D>().gravityScale = 0;
                EventManager.Event_GameOver();
                EventManager.Event_WinLose(false);
            }
            else
            {
                StopCoroutine("ProtectCountDown");
                StartCoroutine("ProtectCountDown", ConstantManager.Data.ConfigData.StunTime);
            }
        }
    }

    private void UpdateHP()
    {
        if (HP <= Hearts.Length)
        {
            Hearts[HP].SetActive(false);
        }
    }

    IEnumerator ProtectCountDown(float TimeCount)
    {
        yield return new WaitForSeconds(TimeCount);
        CanHurt = true;
    }

    private void UpdateTakeStar()
    {
        StarsLine.fillAmount = (float)TakeStars / (float)MaxStars;
        StarsCount.text = TakeStars + "/" + MaxStars;
    }

    private void UpdateCoin()
    {
        foreach (var _CurrentCoin in CurrentCoin)
        {
            _CurrentCoin.text = ConstantManager.Data.PlayeData.Coin.ToString();
        }
    }

    private void UpdateKey()
    {
        CurrentKey.text = KeyValue.ToString();
    }

    private void TakeStar()
    {
        TakeStars++;
        EventManager.Event_UpdateTakeStar();
    }

    private void TakeCoin()
    {
        ConstantManager.Data.PlayeData.Coin += ConstantManager.Data.ConfigData.PointPerCoin;
        EventManager.Event_UpdateCoin();
    }

    private void TakeKey()
    {
        KeyValue += 1;
        EventManager.Event_UpdateKey();
    }

    private string ConverTime_SecondToMinute(int Second)
    {
        int FullSecond = Second;
        int M = (FullSecond - (FullSecond % 60)) / 60;
        int S = FullSecond - (M * 60);
        return M.ToString("00") + ":" + S.ToString("00");
    }

    private void SelectStage(int _StageID)
    {
        StageIDIsLoad = _StageID;
        LoadStageContent.gameObject.SetActive(false);
        if (LoadStageContent.childCount > 0)
        {
            for (int index = 0; index < LoadStageContent.childCount; index++)
            {
                Destroy(LoadStageContent.GetChild(index).gameObject);
            }
        }
        GameObject StageLoad = (GameObject)Instantiate(Stages[_StageID], LoadStageContent);
        LoadStageContent.gameObject.SetActive(true);
        MainPanel.SetActive(false);
        WinLosePanel.SetActive(false);
        PlayPanel.SetActive(true);
    }

    public void SetUpStage(StageCTRL _Stage)
    {
        //StageOnLoad = _Stage;
    }
}
