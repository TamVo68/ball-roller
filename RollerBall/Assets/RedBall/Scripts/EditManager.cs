﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class EditManager : MonoBehaviour
{
    [Header("Main--------------------")]
    [SerializeField] private GameObject EditPanel;
    [SerializeField] private Button ConfigBTN;
    [SerializeField] private Button[] CancelBTNs;

    [Header("Config------------------")]
    [SerializeField] private GameObject ConfigPanel;
    [SerializeField] private TMP_InputField Player_Velocity;
    [SerializeField] private TMP_InputField Player_MaxVelocity;
    [SerializeField] private TMP_InputField Player_JumpStrenght;
    [SerializeField] private TMP_InputField Player_Gravity;
    [SerializeField] private TMP_InputField Stun_Time;
    [SerializeField] private Button Config_Ok;

    // Start is called before the first frame update
    void Start()
    {
        ConfigBTN.onClick.AddListener(()=>
        {
            EditPanel.SetActive(true);
            OpenConfig();
        });

        foreach(Button CancelBTN in CancelBTNs)
        {
            CancelBTN.onClick.AddListener(() => 
            {
                CloseAllPanel();
            });
        }

        Config_Ok.onClick.AddListener(() => 
        {
            CloseAllPanel();
            //ConstantManager.Data.ConfigData.Velocity = float.Parse(Player_Velocity.text);
            ConstantManager.Data.ConfigData.JumpStrenght = float.Parse(Player_JumpStrenght.text);
            ConstantManager.Data.ConfigData.Gravity = float.Parse(Player_Gravity.text);
            ConstantManager.Data.ConfigData.StunTime = float.Parse(Stun_Time.text);
            EventManager.Event_Update_PlayerState();
        });
    }

    private void CloseAllPanel()
    {
        EditPanel.SetActive(false);
        ConfigPanel.SetActive(false);
    }

    private void OpenConfig()
    {
        ConfigPanel.SetActive(true);
        //Player_Velocity.text = ConstantManager.Data.ConfigData.Velocity.ToString();
        Player_JumpStrenght.text = ConstantManager.Data.ConfigData.JumpStrenght.ToString();
        Stun_Time.text = ConstantManager.Data.ConfigData.StunTime.ToString();
        Player_Gravity.text = ConstantManager.Data.ConfigData.Gravity.ToString();
    }
}
