﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterCTRL : MonoBehaviour
{
    private Transform StartPoint;
    public GameObject PlayerAvata;
    public PlayerState State;

    private void Awake()
    {
        EventManager.Event_Update_PlayerState += UpdatePlayerState;
    }

    private void OnDestroy()
    {
        EventManager.Event_Update_PlayerState -= UpdatePlayerState;
    }

    public void SetStartPoint(Transform _Start)
    {
        StartPoint = _Start;
    }

    public void Respawn()
    {
        gameObject.GetComponent<CharacterCTRL>().State = PlayerState.LIVE;
        PlayerAvata.SetActive(true);
        gameObject.GetComponent<Rigidbody2D>().gravityScale = ConstantManager.Data.ConfigData.Gravity;
        transform.position = StartPoint.position;
        State = PlayerState.LIVE;
    }

    private void UpdatePlayerState()
    {
        gameObject.GetComponent<Rigidbody2D>().gravityScale = ConstantManager.Data.ConfigData.Gravity;
    }
}
