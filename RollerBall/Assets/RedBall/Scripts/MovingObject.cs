﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MovingObject : MonoBehaviour
{
    // Start is called before the first frame update
    public MoveType MovingStyle;
    public float MoveRange;
    public float ForwardSpeed;
    public float BackwardSpeed;
    public bool AutoRotation;
    public float RotateSpeed;
    void Start()
    {
        CheckMoveType(MovingStyle);
        if (AutoRotation)
        {
            StopCoroutine("AutoRotate");
            StartCoroutine("AutoRotate");
        }
    }

    private void CheckMoveType(MoveType _MovingStyle)
    {
        //transform.DOKill();
        switch (_MovingStyle)
        {
            case MoveType.STAND:
                {
                    break;
                }
            case MoveType.PATROL:
                {
                    ValueMovement Movement = new ValueMovement();
                    Movement.TimeTweenForward = (MoveRange * 1.5f) / ForwardSpeed;
                    Movement.TimeTweenBackward = (MoveRange * 1.5f) / BackwardSpeed;
                    Movement.StartPos = transform.position.x - ((MoveRange * 1.5f) / 2f);
                    Movement.EndPos = transform.position.x + ((MoveRange * 1.5f) / 2f);
                    StopCoroutine("MoveLeftRight");
                    StartCoroutine("MoveLeftRight", Movement);
                    break;
                }
            case MoveType.RIGHT_BACK:
                {
                    ValueMovement Movement = new ValueMovement();
                    Movement.TimeTweenForward = (MoveRange * 1.5f) / ForwardSpeed;
                    Movement.TimeTweenBackward = (MoveRange * 1.5f) / BackwardSpeed;
                    Movement.StartPos = transform.position.x;
                    Movement.EndPos = transform.position.x + MoveRange * 1.5f;
                    StopCoroutine("MoveLeftRight");
                    StartCoroutine("MoveLeftRight", Movement);
                    break;
                }
            case MoveType.LEFT_BACK:
                {
                    ValueMovement Movement = new ValueMovement();
                    Movement.TimeTweenForward = (MoveRange * 1.5f) / ForwardSpeed;
                    Movement.TimeTweenBackward = (MoveRange * 1.5f) / BackwardSpeed;
                    Movement.StartPos = transform.position.x - MoveRange * 1.5f;
                    Movement.EndPos = transform.position.x;
                    StopCoroutine("MoveLeftRight");
                    StartCoroutine("MoveLeftRight", Movement);
                    break;
                }
            case MoveType.UP_DOWN:
                {
                    ValueMovement Movement = new ValueMovement();
                    Movement.TimeTweenForward = (MoveRange * 1.5f) / ForwardSpeed;
                    Movement.TimeTweenBackward = (MoveRange * 1.5f) / BackwardSpeed;
                    Movement.StartPos = transform.position.y;
                    Movement.EndPos = transform.position.y - MoveRange * 1.5f;
                    StopCoroutine("MoveUpDown");
                    StartCoroutine("MoveUpDown", Movement);
                    break;
                }
        }
    }

    private IEnumerator AutoRotate()
    {
        Rigidbody2D rb = transform.GetComponent<Rigidbody2D>();
        while (true)
        {
            rb.angularVelocity = 360;
            rb.AddTorque(RotateSpeed * Time.deltaTime);
            yield return null;
        }
    }

    private IEnumerator MoveLeftRight(ValueMovement _MovementValue)
    {
        Rigidbody2D rb = transform.GetComponent<Rigidbody2D>();
        while (true)
        {
            while (transform.position.x <= _MovementValue.EndPos)
            {
                rb.MovePosition(new Vector2(transform.position.x + (Time.deltaTime * ForwardSpeed), transform.position.y));
                yield return null;
            }

            while (transform.position.x >= _MovementValue.StartPos)
            {
                rb.MovePosition(new Vector2(transform.position.x - (Time.deltaTime * BackwardSpeed), transform.position.y));
                yield return null;
            }
            yield return null;
        }
    }

    private IEnumerator MoveUpDown(ValueMovement _MovementValue)
    {
        while (true)
        {
            Rigidbody2D rb = transform.GetComponent<Rigidbody2D>();
            while (true)
            {
                while (transform.position.y >= _MovementValue.EndPos)
                {
                    rb.MovePosition(new Vector2(transform.position.x, transform.position.y - (Time.deltaTime * ForwardSpeed)));
                    yield return null;
                }

                while (transform.position.y <= _MovementValue.StartPos)
                {
                    rb.MovePosition(new Vector2(transform.position.x, transform.position.y + (Time.deltaTime * BackwardSpeed)));
                    yield return null;
                }
                yield return null;
            }
        }
    }
}
