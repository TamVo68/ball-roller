﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DragonBones;

public class MoveControl : MonoBehaviour
{
    [Header("Set Up----------------------")]
    [SerializeField] private GameObject Player;
    [SerializeField] private UnityEngine.Transform CheckPoint;
    [SerializeField] private UnityArmatureComponent FaceAnim;
    [SerializeField] private LayerMask GroundLayer;
    [Header("Play------------------------")]
    [SerializeField] private bool CanJump;
    [SerializeField] private bool CanMove;
    [SerializeField] private bool OnWinZone;
    [SerializeField] private TerrianType TerrianState;
    [SerializeField] private WallType WallLeftState;
    [SerializeField] private WallType WallRightState;
    [SerializeField] private int FaceTurn;
    [SerializeField] private Vector2 PlayerVelocity;
    [SerializeField] private float TerrianAngle;

    [SerializeField] private float Agility;
    [SerializeField] private float TimeActiveOnAir;

    private void Awake()
    {
        EventManager.Event_GameOver += GameOver;
    }

    private void OnDestroy()
    {
        EventManager.Event_GameOver -= GameOver;
    }

    private void OnEnable()
    {
        FaceAnim.animation.Play("BinhThuong");
    }

    public void SetStartPoint(UnityEngine.Transform _CheckPoint)
    {
        CheckPoint = _CheckPoint;
    }

    public void SetUpControl(Button MoveLBTN, Button MoveRBTN, Button JumpBTN, GameObject _Player)
    {
        EventTrigger.Entry MoveLeftDown_Entry = new EventTrigger.Entry();
        MoveLeftDown_Entry.eventID = EventTriggerType.PointerDown;
        MoveLeftDown_Entry.callback.AddListener((eventData) => 
        {
            StopCoroutine("MoveLeft");
            StopCoroutine("MoveRight");
            StopCoroutine("SlowDown");
            StartCoroutine("MoveLeft", -1);
        });

        EventTrigger.Entry MoveLeftUp_Entry = new EventTrigger.Entry();
        MoveLeftUp_Entry.eventID = EventTriggerType.PointerUp;
        MoveLeftUp_Entry.callback.AddListener((eventData) => 
        {
            StopCoroutine("MoveLeft");
            StopCoroutine("SlowDown");
            StartCoroutine("SlowDown");
        });

        EventTrigger.Entry MoveRightDown_Entry = new EventTrigger.Entry();
        MoveRightDown_Entry.eventID = EventTriggerType.PointerDown;
        MoveRightDown_Entry.callback.AddListener((eventData) => 
        {
            StopCoroutine("MoveRight");
            StopCoroutine("MoveLeft");
            StopCoroutine("SlowDown");
            StartCoroutine("MoveRight", 1);
        });

        EventTrigger.Entry MoveRightUp_Entry = new EventTrigger.Entry();
        MoveRightUp_Entry.eventID = EventTriggerType.PointerUp;
        MoveRightUp_Entry.callback.AddListener((eventData) => 
        {
            StopCoroutine("MoveRight");
            StopCoroutine("SlowDown");
            //StartCoroutine("SlowDown");
        });

        EventTrigger.Entry JumpDown_Entry = new EventTrigger.Entry();
        JumpDown_Entry.eventID = EventTriggerType.PointerDown;
        JumpDown_Entry.callback.AddListener((eventData) => 
        {
            StopCoroutine("CallJump");
            StartCoroutine("CallJump");
        });

        EventTrigger.Entry JumpUp_Entry = new EventTrigger.Entry();
        JumpUp_Entry.eventID = EventTriggerType.PointerUp;
        JumpUp_Entry.callback.AddListener((eventData) =>
        {
            StopCoroutine("CallJump");
        });

        MoveLBTN.GetComponent<EventTrigger>().triggers.Add(MoveLeftDown_Entry);
        MoveLBTN.GetComponent<EventTrigger>().triggers.Add(MoveLeftUp_Entry);
        MoveRBTN.GetComponent<EventTrigger>().triggers.Add(MoveRightDown_Entry);
        MoveRBTN.GetComponent<EventTrigger>().triggers.Add(MoveRightUp_Entry);
        JumpBTN.GetComponent<EventTrigger>().triggers.Add(JumpDown_Entry);
        JumpBTN.GetComponent<EventTrigger>().triggers.Add(JumpUp_Entry);
        Player = _Player;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            StopCoroutine("MoveRight");
            StopCoroutine("MoveLeft");
            StopCoroutine("SlowDown");
            StartCoroutine("MoveLeft", -1);
        }
        
        if (Input.GetKeyUp(KeyCode.LeftArrow))
        {
            StopCoroutine("MoveLeft");
            StopCoroutine("SlowDown");
            StartCoroutine("SlowDown");
        }

        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            StopCoroutine("MoveLeft");
            StopCoroutine("MoveRight");
            StopCoroutine("SlowDown");
            StartCoroutine("MoveRight", 1);
        }

        if (Input.GetKeyUp(KeyCode.RightArrow))
        {
            StopCoroutine("MoveRight");
            StopCoroutine("SlowDown");
            StartCoroutine("SlowDown");
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            StopCoroutine("CallJump");
            StartCoroutine("CallJump");
        }

        if (Input.GetKeyUp(KeyCode.Space))
        {
            StopCoroutine("CallJump");
        }
    }

    private IEnumerator MoveLeft(int _FaceTurn)
    {
        if (CanMove)
        {
            Rigidbody2D rb = Player.GetComponent<Rigidbody2D>();
            FaceTurn = _FaceTurn;
            PlayerVelocity.x = rb.velocity.x;
            Agility = rb.velocity.x;
            TimeActiveOnAir = 0;
            while (true)
            {
                if (gameObject.GetComponent<CharacterCTRL>().State == PlayerState.LIVE)
                {
                    if ((WallLeftState == WallType.NONE || WallLeftState == WallType.SLOPE) && (TerrianState == TerrianType.GROUND || TerrianState == TerrianType.OBJECT))
                    {
                        
                    }
                    else if (WallLeftState == WallType.NONE && TerrianState == TerrianType.AIR)
                    {
                        Debug.Log("Move In Air");
                        if (Mathf.Abs(rb.velocity.x) < ConstantManager.Data.ConfigData.Max_Air_Velocity)
                        {
                            rb.AddForce(new Vector2(FaceTurn * ConstantManager.Data.ConfigData.Air_Velocity, 0));
                        }
                    }
                    else if (WallLeftState == WallType.WALL)
                    {
                        Debug.Log("Stop");
                    }

                    Debug.Log("Rolling");
                    rb.angularVelocity = -_FaceTurn * ConstantManager.Data.ConfigData.Spin_Speed;
                    if (Mathf.Abs(Agility) <= ConstantManager.Data.ConfigData.Rolling_Velocity)
                    {
                        Agility += FaceTurn * (Time.deltaTime * ConstantManager.Data.ConfigData.Rolling_Velocity);
                    }
                    else
                    {
                        Agility = FaceTurn * ConstantManager.Data.ConfigData.Rolling_Velocity;
                    }
                    rb.AddTorque(FaceTurn * Agility);
                }
                yield return null;
            }
        }
        yield return null;
    }

    private IEnumerator MoveRight(int _FaceTurn)
    {
        if (CanMove)
        {
            Rigidbody2D rb = Player.GetComponent<Rigidbody2D>();
            FaceTurn = _FaceTurn;
            PlayerVelocity.x = rb.velocity.x;
            Agility = rb.velocity.x;
            TimeActiveOnAir = 0;
            while (true)
            {
                if (gameObject.GetComponent<CharacterCTRL>().State == PlayerState.LIVE)
                {
                    if ((WallRightState == WallType.NONE || WallRightState == WallType.SLOPE) && (TerrianState == TerrianType.GROUND || TerrianState == TerrianType.OBJECT))
                    {

                    }
                    else if (WallRightState == WallType.NONE && TerrianState == TerrianType.AIR)
                    {
                        if (Mathf.Abs(rb.velocity.x) < ConstantManager.Data.ConfigData.Max_Air_Velocity)
                        {
                            rb.AddForce(new Vector2(FaceTurn * ConstantManager.Data.ConfigData.Air_Velocity, 0), ForceMode2D.Force);
                        }
                    }
                    else if (WallRightState == WallType.WALL)
                    {
                        Debug.Log("Stop");
                    }

                    rb.angularVelocity = -_FaceTurn * ConstantManager.Data.ConfigData.Spin_Speed;
                    if (Mathf.Abs(Agility) <= ConstantManager.Data.ConfigData.Rolling_Velocity)
                    {
                        Agility += FaceTurn * (Time.deltaTime * ConstantManager.Data.ConfigData.Rolling_Velocity);
                    }
                    else
                    {
                        Agility = FaceTurn * ConstantManager.Data.ConfigData.Rolling_Velocity;
                    }
                    rb.AddTorque(-FaceTurn * Agility);
                }
                yield return null;
            }
        }
        yield return null;
    }

    private void Jump()
    {
        if (CanMove && (TerrianState == TerrianType.GROUND || TerrianState == TerrianType.OBJECT))
        {
            Rigidbody2D rb = Player.GetComponent<Rigidbody2D>();
            Vector2 Movement = new Vector3(FaceTurn, ConstantManager.Data.ConfigData.JumpStrenght);
            rb.AddForce(Movement, ForceMode2D.Impulse);
            //Debug.Log("jump high: " + rb.velocity.magnitude);
            TerrianState = TerrianType.AIR;
        }
        if (OnWinZone && ConstantManager.Game_Manager.CheckAlStarClear())
        {
            RaycastHit2D[] Hit = Physics2D.RaycastAll(transform.position, Vector2.down, gameObject.GetComponent<CircleCollider2D>().radius * 0.9f);
            foreach (var _Win in Hit)
            {
                if (_Win.transform.CompareTag("WinArea"))
                {
                    EventManager.Event_WinLose(true);
                }
            }
        }
    }

    private IEnumerator CallJump()
    {
        //while (true)
        //{
        //    if (TerrianState == TerrianType.GROUND || TerrianState == TerrianType.OBJECT)
        //    { 
        //        Jump();
        //    }
        //    yield return null;//new WaitForSeconds (0.05f);
        //}
        if (TerrianState == TerrianType.GROUND || TerrianState == TerrianType.OBJECT)
        {
            Jump();
        }
        yield return new WaitForSeconds (0.05f);
    }

    private void SpringJump()
    {
        Rigidbody2D rb = gameObject.GetComponent<Rigidbody2D>();
        Debug.Log("Spring Jump");
        rb.AddForce(new Vector2(0, ConstantManager.Data.ConfigData.JumpStrenght * ConstantManager.Data.ConfigData.SpringJumpStrenght));
    }

    private IEnumerator SlowDown()
    {
        float Velocity;
        Rigidbody2D rb = Player.GetComponent<Rigidbody2D>();
        while (true)
        {
            Velocity = rb.velocity.x;
            if (Mathf.Abs(rb.velocity.x) > 0)
            {
                Velocity -= Velocity * Time.deltaTime * 2;
                rb.velocity = new Vector2(Velocity, rb.velocity.y);
            }
            else
            {
                break;
            }
            yield return null;
        }
    }

    private void Movement(int FaceTurn)
    {
        //Debug.Log("Movement");
        //Rigidbody2D rb = Player.GetComponent<Rigidbody2D>();
        //if (TerrianState == TerrianType.GROUND && WallState == WallType.SLOPE && TerrianAngle <= 45)
        //{
        //    Debug.Log("Climb");
        //    PlayerVelocity.Set(ConstantManager.Data.ConfigData.Rolling_Velocity * FaceTurn, rb.velocity.y);
        //    //rb.velocity = PlayerVelocity;
        //}
        //else if (TerrianState == TerrianType.AIR && WallState == WallType.NONE)
        //{
        //    Debug.Log("Move on Air");
        //    PlayerVelocity.Set(ConstantManager.Data.ConfigData.Rolling_Velocity * FaceTurn, rb.velocity.y);
        //    rb.velocity = PlayerVelocity;
        //}
        //else if ((TerrianState == TerrianType.GROUND || TerrianState == TerrianType.OBJECT))
        //{
        //    Debug.Log("Rolling");
        //    PlayerVelocity.Set(FaceTurn * ConstantManager.Data.ConfigData.Rolling_Velocity, rb.velocity.y);
        //    rb.velocity = PlayerVelocity;
        //}
    }

    private IEnumerator Impact(Vector3 TargetPos)
    {
        Rigidbody2D rb = Player.GetComponent<Rigidbody2D>();
        float Strength = transform.position.x - TargetPos.x;
        float High = transform.position.y - TargetPos.y;
        Vector2 ImpactStrenght;
        if (Strength < 0)
        {
            ImpactStrenght = new Vector2(-Strength - 10, ConstantManager.Data.ConfigData.ImpactHigh);
        }
        else
        {
            ImpactStrenght = new Vector2(-Strength + 10, ConstantManager.Data.ConfigData.ImpactHigh);
        }
        rb.AddForce(ImpactStrenght, ForceMode2D.Impulse);
        yield return null;
    }

    private IEnumerator Tomb(float TombStrenght)
    {
        Rigidbody2D rb = Player.GetComponent<Rigidbody2D>();
        Vector2 Movement = new Vector3(0, TombStrenght);
        rb.AddForce(Movement, ForceMode2D.Impulse);
        yield return null;
    }

    private void GameOver()
    {
        StopCoroutine("Impact");
        StopCoroutine("PlayFace");
        gameObject.GetComponent<CharacterCTRL>().State = PlayerState.DEAD;
    }

    private void CheckTerrian(Collision2D collision)
    {
        RaycastHit2D[] hit = Physics2D.RaycastAll(transform.position, Vector2.down, gameObject.GetComponent<CircleCollider2D>().radius * 1f);
        Debug.DrawRay(transform.position, (Vector2.down + new Vector2(0, gameObject.GetComponent<CircleCollider2D>().radius * 1f)), new Color(255, 0, 0, 255));
        for (int index = 0; index < hit.Length; index++)
        {
            if (hit[index].transform.CompareTag("Ground"))
            {
                TerrianState = TerrianType.GROUND;
            }
            else if (hit[index].transform.CompareTag("Door") || hit[index].transform.CompareTag("Crate") || hit[index].transform.CompareTag("Prison") || hit[index].transform.CompareTag("MovingPlatform") || hit[index].transform.CompareTag("Hurt"))
            {
                TerrianState = TerrianType.OBJECT;
            }
            else
            {
                TerrianState = TerrianType.AIR;
            }
        }
    }

    private void CheckWall(Collision2D collision)
    {
        if (collision.contactCount > 0)
        {
            for (int index = 0; index < collision.contactCount; index++)
            {
                RaycastHit2D[] hit_left = Physics2D.RaycastAll(transform.position, new Vector2(-1.2f,-1f), gameObject.GetComponent<CircleCollider2D>().radius * 1f);
                for (int index1 = 0; index1 < hit_left.Length; index1++)
                {
                    if (hit_left[index1].transform.CompareTag("Ground") || hit_left[index1].transform.CompareTag("Door") || hit_left[index1].transform.CompareTag("Crate") || hit_left[index1].transform.CompareTag("Prison"))
                    {
                        Debug.Log("Angle Left" + "[" + index1 + "]: " + hit_left[index1].transform.name); //Vector2.Angle(Vector2.up, hit_left[index1].normal));
                        TerrianAngle = Vector2.Angle(Vector2.up, hit_left[index1].normal);
                        if (Vector2.Angle(Vector2.up, hit_left[index1].normal) == ConstantManager.Data.ConfigData.Max_TerrianAngle)
                        {
                            WallLeftState = WallType.WALL;
                            break;
                        }
                        else if (Vector2.Angle(Vector2.up, hit_left[index1].normal) > 1 && Vector2.Angle(Vector2.up, hit_left[index1].normal) <= 55)
                        {
                            WallLeftState = WallType.SLOPE;
                            break;
                        }
                        else
                        {
                            TerrianAngle = 0;
                            WallLeftState = WallType.NONE;
                            break;
                        }
                    }
                }

                RaycastHit2D[] hit_right = Physics2D.RaycastAll(transform.position, new Vector2(1.2f, -1f), gameObject.GetComponent<CircleCollider2D>().radius * 1f);
                for (int index1 = 0; index1 < hit_right.Length; index1++)
                {
                    if (hit_right[index1].transform.CompareTag("Ground") || hit_right[index1].transform.CompareTag("Door"))
                    {
                        Debug.Log("Angle Right" + "[" + index1 + "]: " + Vector2.Angle(Vector2.up, hit_right[index1].normal));
                        TerrianAngle = Mathf.RoundToInt(Vector2.Angle(Vector2.up, hit_right[index1].normal));
                        if (Vector2.Angle(Vector2.up, hit_right[index1].normal) == ConstantManager.Data.ConfigData.Max_TerrianAngle)
                        {
                            WallRightState = WallType.WALL;
                            break;
                        }
                        else if (Vector2.Angle(Vector2.up, hit_right[index1].normal) > 1 && Vector2.Angle(Vector2.up, hit_right[index1].normal) <= 55)
                        {
                            WallRightState = WallType.SLOPE;
                            break;
                        }
                        else
                        {
                            TerrianAngle = 0;
                            WallRightState = WallType.NONE;
                            break;
                        }
                    }
                }
            }
        }
        else
        {
            TerrianAngle = 0;
            WallLeftState = WallType.NONE;
            WallRightState = WallType.NONE;
        }

        //RaycastHit2D[] Walls;
        //if (FaceTurn < 0)
        //{
        //    Walls = Physics2D.RaycastAll(transform.position, Vector2.left, gameObject.GetComponent<CircleCollider2D>().radius * 1f);
        //}
        //else
        //{
        //    Walls = Physics2D.RaycastAll(transform.position, Vector2.right, gameObject.GetComponent<CircleCollider2D>().radius * 1f);
        //}

        //for (int index = 0; index < Walls.Length; index++)
        //{
        //    if (Walls[index].transform.CompareTag("Ground") || Walls[index].transform.CompareTag("Door") || Walls[index].transform.CompareTag("Crate") || Walls[index].transform.CompareTag("Prison"))
        //    {
        //        WallState = WallType.WALL;
        //        TerrianAngle = Vector2.Angle(Vector2.up, Walls[index].normal);
        //        break;
        //    }
        //    else
        //    {
        //        WallState = WallType.NONE;
        //    }
        //}
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        CheckTerrian(collision);
        CheckWall(collision);
        //if (collision.transform.CompareTag("MovingPlatform"))
        //{
        //    transform.SetParent(collision.transform);
        //}
        RaycastHit2D[] Hit = Physics2D.RaycastAll(transform.position, Vector2.down, gameObject.GetComponent<CircleCollider2D>().radius * 1.2f);
        List<UnityEngine.Transform> ObjectCanDestroy = new List<UnityEngine.Transform>();
        foreach (var OBJ in Hit)
        {
            switch (OBJ.transform.tag)
            {
                case "Enermy":
                    {
                        ObjectCanDestroy.Add(OBJ.transform);
                        break;
                    }
                case "Prison":
                    {
                        ObjectCanDestroy.Add(OBJ.transform);
                        break;
                    }
                case "Spring":
                    {
                        SpringJump();
                        break;
                    }
            }
        }

        if (ObjectCanDestroy.Count > 0 && ConstantManager.Game_Manager.CanHurt)
        {
            for (int index = 0; index < ObjectCanDestroy.Count; index++)
            {
                if (ObjectCanDestroy[index].CompareTag("Enermy") && !ObjectCanDestroy[index].GetComponent<Enermy>().Invincible)
                {
                    StartCoroutine("Tomb", ConstantManager.Data.ConfigData.TombStrenght);
                    collision.gameObject.GetComponent<Enermy>().Dead();
                }
                else if (ObjectCanDestroy[index].CompareTag("Enermy") && ObjectCanDestroy[index].GetComponent<Enermy>().Invincible)
                {
                    EventManager.Event_SubHP(1);
                    StopCoroutine("Impact");
                    StartCoroutine("Impact", collision.transform.position);
                    StopCoroutine("PlayFace");
                    StartCoroutine("PlayFace");
                }
                else if (ObjectCanDestroy[index].CompareTag("Prison"))
                {
                    Debug.Log("On Prison");
                }
            }
        }
        else
        {
            if ((collision.gameObject.CompareTag("Enermy") || collision.gameObject.CompareTag("Hurt")) && ConstantManager.Game_Manager.CanHurt)
            {
                EventManager.Event_SubHP(1);
                StopCoroutine("Impact");
                StartCoroutine("Impact", collision.transform.position);
                StopCoroutine("PlayFace");
                StartCoroutine("PlayFace");
            }
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        CheckTerrian(collision);
        CheckWall(collision);
        //if (collision.transform.CompareTag("MovingPlatform"))
        //{
        //    transform.SetParent(null);
        //}
    }

    private IEnumerator PlayFace()
    {
        FaceAnim.animation.Play("TucGian");
        yield return new WaitForSeconds(2f);
        FaceAnim.animation.Play("BinhThuong");
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "DeadZone" && gameObject.GetComponent<CharacterCTRL>().State != PlayerState.DEAD)
        {
            gameObject.GetComponent<CharacterCTRL>().State = PlayerState.DEAD;
            gameObject.GetComponent<Rigidbody2D>().gravityScale = 0;
            EventManager.Event_WinLose(false);
        }
        else if (collision.gameObject.CompareTag("Star") && gameObject.GetComponent<CharacterCTRL>().State != PlayerState.DEAD)
        {
            EventManager.Event_TakeStar();
            collision.gameObject.SetActive(false);
        }
        else if (collision.gameObject.CompareTag("Coin") && gameObject.GetComponent<CharacterCTRL>().State != PlayerState.DEAD)
        {
            EventManager.Event_TakeCoin();
            collision.gameObject.SetActive(false);
        }
        else if (collision.gameObject.CompareTag("Key") && gameObject.GetComponent<CharacterCTRL>().State != PlayerState.DEAD)
        {
            EventManager.Event_TakeKey();
            collision.gameObject.SetActive(false);
        }
        else if (collision.tag == "WinArea")
        {
            OnWinZone = true;
        }
        else if (collision.gameObject.CompareTag("Bullet"))
        {
            EventManager.Event_SubHP(1);
            collision.gameObject.SetActive(false);
            StopCoroutine("Impact");
            StartCoroutine("Impact", collision.transform.position);
            StopCoroutine("PlayFace");
            StartCoroutine("PlayFace");
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "WinArea")
        {
            OnWinZone = false;
        }
    }
}
