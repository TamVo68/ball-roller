﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class StageCTRL : MonoBehaviour
{
    [SerializeField] private Material TileMapMaterial;
    [SerializeField] private TilemapRenderer GroundLayer;
    [SerializeField] private Transform StartPoint;
    [SerializeField] private Transform StarsContents;
    [SerializeField] private int TimeStage_s;

    private void Start()
    {
        ConstantManager.Game_Manager.SetUpStage(this);
        GroundLayer.material = null;
        GroundLayer.material = TileMapMaterial;
        EventManager.Event_StartGame(StartPoint, GroundLayer, StarsContents.childCount, TimeStage_s, StartPoint);
    }
}
