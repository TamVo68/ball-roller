﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class WinDoor : MonoBehaviour
{
    public float OpenSpeed;
    [SerializeField] private GameObject DoorLeft;
    [SerializeField] private GameObject DoorRight;
    [SerializeField] private GameObject Hole;
    public bool IsOpen;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (ConstantManager.Game_Manager.CheckAlStarClear())
            {
                Hole.SetActive(true);
            }
            else
            {
                Hole.SetActive(false);
            }

            DoorLeft.transform.DOKill();
            DoorRight.transform.DOKill();
            OpenDoor();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            DoorLeft.transform.DOKill();
            DoorRight.transform.DOKill();
            CloseDoor();
        }
    }

    private void OpenDoor()
    {
        DoorLeft.transform.DOLocalRotate(new Vector3(0, -90, 0), 10f / OpenSpeed).SetEase(Ease.OutExpo);
        DoorRight.transform.DOLocalRotate(new Vector3(0, 90, 0), 10f / OpenSpeed).SetEase(Ease.OutExpo).OnComplete(() => 
        {
            IsOpen = true;
        });
    }

    private void CloseDoor()
    {
        DoorLeft.transform.DOLocalRotate(new Vector3(0, 0, 0), 10f / OpenSpeed).SetEase(Ease.OutExpo);
        DoorRight.transform.DOLocalRotate(new Vector3(0, 0, 0), 10f / OpenSpeed).SetEase(Ease.OutExpo).OnComplete(()=>
        {
            IsOpen = false;
        });
    }
}
