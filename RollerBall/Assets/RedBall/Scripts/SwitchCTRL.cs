﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class SwitchCTRL : MonoBehaviour
{
    [SerializeField] private SwitchType ControlType;
    [SerializeField] private MachineObject[] Machines;
    [SerializeField] private SwitchCTRL[] Switchs;
    [Header("Button----------------------")]
    public GameObject ButtonKnob;
    [Header("Lever-----------------------")]
    public GameObject HandLever;
    [SerializeField] private float AngleTouch;
    [SerializeField] private float AngleActive;
    [SerializeField] private bool SwitchState;
    private Vector2 StartPos;
    private bool IsActive;

    private void Start()
    {
        if (ButtonKnob)
        {
            StartPos = ButtonKnob.transform.localPosition;
        }
        else if (HandLever)
        {
            if (Machines.Length > 0)
            { }
            gameObject.GetComponent<PlatformEffector2D>().surfaceArc = AngleTouch;
            LeverAnim();
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            HandLever.transform.localEulerAngles = new Vector3(0,0,45);
        }
        else if (Input.GetKeyDown(KeyCode.D))
        {
            HandLever.transform.localEulerAngles = new Vector3(0, 0, -45);
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        switch (ControlType)
        {
            case SwitchType.Button:
                {
                    if (collision.gameObject.CompareTag("Crate") || collision.gameObject.CompareTag("Player"))
                    {
                        if (!SwitchState)
                        {
                            SwitchState = true;
                            ButtonAnim();
                            if (Machines.Length > 0)
                            {
                                foreach (var Machine in Machines)
                                {
                                    Machine.CheckMachineActive(SwitchState);
                                }
                            }

                            if (Switchs.Length > 0)
                            {
                                foreach (var Switch in Switchs)
                                {
                                    Switch.ControlState(SwitchState);
                                }
                            }
                        }
                    }
                    break;
                }
            case SwitchType.Lever:
                {
                    if (collision.gameObject.CompareTag("Untagged"))
                    {
                        HandLever.gameObject.GetComponent<Collider2D>().enabled = false;
                        if (SwitchState)
                        {
                            SwitchState = false;
                        }
                        else
                        {
                            SwitchState = true;
                        }
                        LeverAnim();
                    }
                    break;
                }
        }
    }

    public void ControlState(bool _SwitchState)
    {
        SwitchState = _SwitchState;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        switch (ControlType)
        {
            case SwitchType.Button:
                {
                    if (collision.gameObject.CompareTag("Crate") || collision.gameObject.CompareTag("Player"))
                    {
                        if (SwitchState)
                        {
                            SwitchState = false;
                            ButtonKnob.transform.DOKill();
                            ButtonAnim();
                            if (Machines.Length > 0)
                            {
                                foreach (var Machine in Machines)
                                {
                                    Machine.CheckMachineActive(SwitchState);
                                }
                            }

                            if (Switchs.Length > 0)
                            {
                                foreach (var Switch in Switchs)
                                {
                                    Switch.ControlState(SwitchState);
                                }
                            }
                        }
                    }
                    break;
                }
            case SwitchType.Lever:
                {
                    if (collision.gameObject.CompareTag("Player"))
                    {
                        LeverAnim();
                    }
                    break;
                }
        }
    }

    private void ButtonAnim()
    {
        if (SwitchState)
        {
            ButtonKnob.transform.DOLocalMoveY(-0.02f, 0.3f).SetEase(Ease.Flash);
        }
        else
        {
            ButtonKnob.transform.DOLocalMoveY(StartPos.y, 0.3f).SetEase(Ease.Flash);
        }
    }
    public IEnumerator LeverMirror(Transform OriginalLever)
    {
        while (true)
        {
            HandLever.transform.GetComponent<Rigidbody2D>().MoveRotation(OriginalLever.localRotation.z);
            yield return null;
        }
    }

    private void LeverAnim()
    {
        gameObject.GetComponent<PlatformEffector2D>().enabled = false;
        if (SwitchState)
        {
            HandLever.transform.DORotate(new Vector3(0, 0, -45), 0.3f).SetEase(Ease.Flash).OnComplete(() =>
            {
                gameObject.GetComponent<Collider2D>().isTrigger = false;
                gameObject.GetComponent<PlatformEffector2D>().rotationalOffset = -AngleActive;
                gameObject.GetComponent<Collider2D>().enabled = true;
                if (Machines.Length > 0)
                {
                    foreach (var Machine in Machines)
                    {
                        Machine.CheckMachineActive(SwitchState);
                    }
                }
            });
        }
        else
        {
            HandLever.transform.DORotate(new Vector3(0, 0, 45), 0.3f).SetEase(Ease.Flash).OnComplete(() =>
            {
                gameObject.GetComponent<Collider2D>().isTrigger = false;
                gameObject.GetComponent<PlatformEffector2D>().rotationalOffset = AngleActive;
                gameObject.GetComponent<Collider2D>().enabled = true;
                if (Machines.Length > 0)
                {
                    foreach (var Machine in Machines)
                    {
                        Debug.Log(SwitchState);
                        Machine.CheckMachineActive(SwitchState);
                    }
                }
            });
        }
        gameObject.GetComponent<PlatformEffector2D>().enabled = true;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.CompareTag("Player") && ControlType == SwitchType.Lever)
        {
            for (int index = 0; index < Machines.Length; index++)
            {
                if (Machines[index].M_Type == MachineType.Elevator)
                {
                    Machines[index].Lever.StopCoroutine("LeverMirror");
                    Machines[index].Lever.StartCoroutine("LeverMirror",HandLever.transform.rotation.eulerAngles);
                }
            }
        }
    }


    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.transform.CompareTag("Player") && ControlType == SwitchType.Lever)
        {
            for (int index = 0; index < Machines.Length; index++)
            {
                if (Machines[index].M_Type == MachineType.Elevator)
                {
                    Machines[index].Lever.StopCoroutine("LeverMirror");
                }
            }
        }
    }
}
