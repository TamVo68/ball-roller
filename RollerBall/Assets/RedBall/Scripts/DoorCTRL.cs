﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DoorCTRL : MonoBehaviour
{
    public float RangeOpen;
    public float DoorSpeed;
    [SerializeField] GameObject DoorActive; 
    private bool IsOpen;
    private float StartDoorPos;
    private void Start()
    {
        StartDoorPos = DoorActive.transform.localPosition.y;
    }

    public void OpenCloseDoor(bool _Switch)
    {
        IsOpen = _Switch;
        transform.DOKill();
        StopCoroutine("ActionDoor");
        StartCoroutine("ActionDoor");
    }

    private IEnumerator ActionDoor()
    {
        float TimeSpeed = (RangeOpen * 1.6f) / DoorSpeed;
        if (IsOpen)
        {
            DoorActive.transform.DOLocalMoveY(StartDoorPos - RangeOpen, TimeSpeed).SetEase(Ease.Flash);
        }
        else
        {
            DoorActive.transform.DOLocalMoveY(StartDoorPos, TimeSpeed).SetEase(Ease.Flash);
        }
        yield return null;
    }
}
