﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMonster : MonoBehaviour
{
    [SerializeField] private float Speed;
    private Vector2 VectorShot;

    public void SetSpeed(Vector2 _VectorShot, float _Speed)
    {
        VectorShot = _VectorShot;
        Speed = _Speed;
        StopCoroutine("Movement");
        StartCoroutine("Movement");
    }

    private IEnumerator Movement()
    {
        Vector2 Move = Vector3.zero;
        Vector2 Current;

        gameObject.SetActive(true);
        while (true)
        {
            Current = new Vector2(transform.position.x, transform.position.y);
            Move = new Vector2(transform.position.x + (VectorShot.x * Speed * Time.deltaTime), transform.position.y + (VectorShot.y * Speed * Time.deltaTime));
            transform.position = Vector2.Lerp(Current, Move, Speed * Time.deltaTime);
            yield return new WaitForSeconds(Time.deltaTime);
        }
    }

    //private void OnCollisionEnter2D(Collision2D collision)
    //{
    //    if (collision.gameObject.CompareTag("Ground") || collision.gameObject.CompareTag("DeadZone") || collision.gameObject.CompareTag("Crate"))
    //    {
    //        gameObject.SetActive(false);
    //    }
    //}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Ground") || collision.gameObject.CompareTag("DeadZone") || collision.gameObject.CompareTag("Crate"))
        {
            gameObject.SetActive(false);
        }
    }
}
