﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MachineObject : MonoBehaviour
{
    [Header("Main Set-Up------------------------")]
    public MachineType M_Type;
    [SerializeField] private bool Start_Status;
    [SerializeField] private bool Status;
    [SerializeField] private Rigidbody2D rb;
    [SerializeField] private GameObject[] ActiveParts;

    [Header("SetMovement------------------------")]
    [SerializeField] private MoveType Movement_Type;
    [SerializeField] private float MoveRange;
    [SerializeField] private float ForwardSpeed;
    [SerializeField] private float BackwardSpeed;
    [SerializeField] private float ReActionTime;
    [Header("Support Value----------------------")]
    [SerializeField] private float SpinSpeed;
    [SerializeField] private float SpinAngle;
    [SerializeField] private bool AutoMovement;

    [Header(">Elevator----------")]
    [SerializeField] private GameObject Elevator_Gear;
    [SerializeField] private bool IsMoveDown;
    public SwitchCTRL Lever;
    [Header(">Crusher-----------")]
    [SerializeField] private GameObject MovementSpike;

    [SerializeField] private List<Vector2> StartPos;
    [SerializeField] private List<Vector2> EndPos;

    private void Start()
    {
        StartPos = new List<Vector2>();
        EndPos = new List<Vector2>();
        for (int index = 0; index < ActiveParts.Length; index++)
        {
            StartPos.Add(ActiveParts[index].transform.localPosition);
        }

        for (int index = 0; index < ActiveParts.Length; index++)
        {
            EndPos.Add(new Vector2( ActiveParts[index].transform.position.x + MoveRange, ActiveParts[index].transform.position.y + MoveRange));
        }
        CheckMachineActive(Status);
        CheckAutoMovement();
    }

    public void CheckMachineActive(bool _Status)
    {
        Status = _Status;
        if (Start_Status)
        {
            if (Status)
            {
                DeActive();
            }
            else
            {
                Active();
            }
        }
        else
        {
            if (Status)
            {
                Active();
            }
            else
            {
                DeActive();
            }
        }
    }

    public void Active()
    {
        switch (M_Type)
        {
            case MachineType.Door:
                {
                    StopCoroutine("ActionDoor");
                    StartCoroutine("ActionDoor", true);
                    break;
                }
            case MachineType.LockDoor:
                {
                    StopCoroutine("ActiveDouDoor");
                    StartCoroutine("ActiveDouDoor", true);
                    break;
                }
            case MachineType.DouDoor:
                {
                    StopCoroutine("ActiveDouDoor");
                    StartCoroutine("ActiveDouDoor", true);
                    break;
                }
            case MachineType.Elevator:
                {
                    StopCoroutine("ElevatorMovement");
                    StartCoroutine("ElevatorMovement", true);
                    break;
                }
            case MachineType.Saw:
                {
                    StopCoroutine("Spin");
                    StartCoroutine("Spin");
                    break;
                }
            case MachineType.Spin_Knife:
                {
                    StopCoroutine("Spin");
                    StartCoroutine("Spin");
                    break;
                }
            case MachineType.Crusher:
                {
                    break;
                }
        }
    }

    public void DeActive()
    {
        switch (M_Type)
        {
            case MachineType.Door:
                {
                    StopCoroutine("ActionDoor");
                    StartCoroutine("ActionDoor", false);
                    break;
                }
            case MachineType.LockDoor:
                {
                    StopCoroutine("ActiveDouDoor");
                    StartCoroutine("ActiveDouDoor", false);
                    break;
                }
            case MachineType.DouDoor:
                {
                    StopCoroutine("ActiveDouDoor");
                    StartCoroutine("ActiveDouDoor", false);
                    break;
                }
            case MachineType.Elevator:
                {
                    StopCoroutine("ElevatorMovement");
                    StartCoroutine("ElevatorMovement", false);
                    break;
                }
            case MachineType.Saw:
                {
                    break;
                }
            case MachineType.Spin_Knife:
                {
                    StopCoroutine("Spin");
                    break;
                }
            case MachineType.Crusher:
                {
                    break;
                }
        }
    }

    private void CheckAutoMovement()
    {
        if (AutoMovement)
        {
            switch (Movement_Type)
            {
                case MoveType.PATROL:
                    {
                        for (int index = 0; index < StartPos.Count; index++)
                        { 
                            StartPos[index] = new Vector2(ActiveParts[index].transform.position.x - ((MoveRange / 2)), ActiveParts[index].transform.position.y);
                        }

                        for (int index = 0; index < EndPos.Count; index++)
                        {
                            EndPos[index] = new Vector2(ActiveParts[index].transform.position.x + ((MoveRange / 2)), ActiveParts[index].transform.position.y);
                        }
                        StopCoroutine("MoveLeftRightRepeat");
                        StartCoroutine("MoveLeftRightRepeat");
                        break;
                    }
                case MoveType.LEFT_BACK:
                    {
                        for (int index = 0; index < StartPos.Count; index++)
                        {
                            StartPos[index] = new Vector2(ActiveParts[index].transform.position.x - MoveRange, ActiveParts[index].transform.position.y);
                        }

                        for (int index = 0; index < EndPos.Count; index++)
                        {
                            EndPos[index] = new Vector2(ActiveParts[index].transform.position.x, ActiveParts[index].transform.position.y);
                        }
                        StopCoroutine("MoveLeftRightRepeat");
                        StartCoroutine("MoveLeftRightRepeat");
                        break;
                    }
                case MoveType.RIGHT_BACK:
                    {
                        for (int index = 0; index < StartPos.Count; index++)
                        {
                            StartPos[index] = new Vector2(ActiveParts[index].transform.position.x, ActiveParts[index].transform.position.y);
                        }

                        for (int index = 0; index < EndPos.Count; index++)
                        {
                            EndPos[index] = new Vector2(ActiveParts[index].transform.position.x + MoveRange, ActiveParts[index].transform.position.y);
                        }
                        StopCoroutine("MoveLeftRightRepeat");
                        StartCoroutine("MoveLeftRightRepeat");
                        break;
                    }
                case MoveType.UP_DOWN:
                    {
                        StopCoroutine("MoveUpDownRepeat");
                        StartCoroutine("MoveUpDownRepeat");
                        break;
                    }
            }
        }
    }

    private IEnumerator Spin()
    {
        while (true)
        {
            rb.angularVelocity = SpinAngle;
            rb.AddTorque(SpinSpeed * Time.deltaTime);
            yield return null;
        }
    }

    private IEnumerator ActionDoor(bool _Status)
    {
        for (int index = 0; index < ActiveParts.Length; index++)
        {
            if (_Status)
            {
                ActiveParts[index].transform.DOLocalMoveY(StartPos[index].y - MoveRange, (MoveRange * 1.6f) / ForwardSpeed).SetEase(Ease.Flash);
            }
            else
            {
                ActiveParts[index].transform.DOLocalMoveY(StartPos[index].y, (MoveRange * 1.6f) / BackwardSpeed).SetEase(Ease.Flash);
            }
        }
        yield return null;
    }

    private IEnumerator ActiveDouDoor(bool _Status)
    {
        for (int index = 0; index < ActiveParts.Length; index++)
        {
            if (_Status)
            {
                if (index % 2 == 0)
                {
                    ActiveParts[index].transform.DOLocalMoveY(StartPos[index].y + MoveRange, (MoveRange * 1.6f) / ForwardSpeed).SetEase(Ease.Flash);
                }
                else
                {
                    ActiveParts[index].transform.DOLocalMoveY(StartPos[index].y - MoveRange, (MoveRange * 1.6f) / ForwardSpeed).SetEase(Ease.Flash);
                }
            }
            else
            {
                ActiveParts[index].transform.DOLocalMoveY(StartPos[index].y, (MoveRange * 1.6f) / BackwardSpeed).SetEase(Ease.Flash);
            }
        }
        yield return null;
    }

    private IEnumerator MoveLeftRightRepeat()
    {
        while (true)
        {
            for (int index = 0; index < ActiveParts.Length; index++)
            {
                while (transform.position.x <= EndPos[index].x)
                {
                    rb.MovePosition(new Vector2(transform.position.x + (Time.deltaTime * ForwardSpeed), transform.position.y));
                    yield return null;
                }

                while (transform.position.x >= StartPos[index].x)
                {
                    rb.MovePosition(new Vector2(transform.position.x - (Time.deltaTime * BackwardSpeed), transform.position.y));
                    yield return null;
                }
                yield return null;
            }
        }
    }

    private IEnumerator MoveUpDownRepeat()
    {
        while (true)
        {
            for (int index = 0; index < ActiveParts.Length; index++)
            {
                while (transform.position.y <= EndPos[index].y)
                {
                    rb.MovePosition(new Vector2(transform.position.x, transform.position.y + (Time.deltaTime * ForwardSpeed)));
                    yield return null;
                }

                while (transform.position.y >= StartPos[index].y)
                {
                    rb.MovePosition(new Vector2(transform.position.x, transform.position.y - (Time.deltaTime * BackwardSpeed)));
                    yield return null;
                }
                yield return null;
            }
        }
    }

    private IEnumerator ElevatorMovement(bool _Status)
    {
        for (int index = 0; index < ActiveParts.Length; index++)
        {
            if (_Status)
            {
                ActiveParts[index].transform.DOLocalMoveY(StartPos[index].y + MoveRange, (Mathf.Abs(MoveRange) * 1.6f) / ForwardSpeed).SetEase(Ease.Flash);
            }
            else
            {
                ActiveParts[index].transform.DOLocalMoveY(StartPos[index].y, (Mathf.Abs(MoveRange) * 1.6f) / BackwardSpeed).SetEase(Ease.Flash);
            }
        }
        yield return null;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (ConstantManager.Game_Manager.KeyValue > 0 && M_Type == MachineType.LockDoor && !Status)
        {
            ConstantManager.Game_Manager.KeyValue -= 1;
            EventManager.Event_UpdateKey();
            StopCoroutine("ActiveDouDoor");
            StartCoroutine("ActiveDouDoor", true);
        }
    }
}
