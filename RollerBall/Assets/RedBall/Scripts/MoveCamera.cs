﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class MoveCamera : MonoBehaviour
{
    [Header("Main---------------------")]
    [SerializeField] private TilemapRenderer TileSize;
    [SerializeField] private Camera PlayerCam;
    [SerializeField] private GameObject Player;
    [SerializeField] private float SmoothFactor;

    [Header("Value--------------------")]
    [SerializeField] private Bounds MapSize;
    [SerializeField] private Vector2 CamCenter;

    [Header("Camera Edit Focus")]
    public float Focus_X = 0f;
    public float Focus_Y = 1.65f;


    private bool Follow;

    private void Awake()
    {
        EventManager.Event_GameOver += GameOver;
    }

    private void OnDestroy()
    {
        EventManager.Event_GameOver -= GameOver;
    }

    private void Start()
    {
        Boundsxtension.Cam = PlayerCam;
        
        Follow = true;
        #region Camcenter Calculate
        float x = (Mathf.Abs(Boundsxtension.OrthographicBounds().min.x) + Mathf.Abs(Boundsxtension.OrthographicBounds().max.x)) / 2;
        float y = (Mathf.Abs(Boundsxtension.OrthographicBounds().min.y) + Mathf.Abs(Boundsxtension.OrthographicBounds().min.y)) / 2;
        CamCenter = new Vector2(x, y);
        #endregion
    }

    private void Update()
    {
        MoveFollow();
    }

    public void SetControl(GameObject _Player, TilemapRenderer _TileSize)
    {
        Player = _Player;
        TileSize = _TileSize;
        MapSize = TileSize.bounds;
        MapSize.size = MapSize.size;
    }

    private void GameOver()
    {
        Follow = false;
    }

    public void StartFollow()
    {
        Follow = true;
    }

    private void MoveFollow()
    {
        if (Player)
        {
            if (Follow)
            {
                Vector3 NewPos = new Vector3(Player.transform.position.x, Player.transform.position.y, PlayerCam.transform.position.z);

                if (MapSize.min.x >= NewPos.x - CamCenter.x - Focus_X)
                {
                    NewPos.x = MapSize.min.x + CamCenter.x;
                }
                else if (MapSize.max.x <= NewPos.x + CamCenter.x - Focus_X)
                {
                    NewPos.x = MapSize.max.x - CamCenter.x;
                }
                else
                {
                    if(Mathf.Abs(NewPos.x) > CamCenter.x)
                        NewPos.x += 0.8f;
                }

                if (MapSize.min.y >= NewPos.y - CamCenter.y + Focus_Y)
                {
                    NewPos.y = MapSize.min.y + CamCenter.y - Focus_Y;
                }
                else if (MapSize.max.y <= NewPos.y + CamCenter.y - Focus_Y)
                {
                    NewPos.y = MapSize.max.y - CamCenter.y + Focus_Y;
                }

                PlayerCam.transform.position = Vector3.Lerp(PlayerCam.transform.position, NewPos, SmoothFactor * Time.deltaTime);
            }
        }
    }
}

public static class Boundsxtension
{
    public static Camera Cam;
    public static bool ContainBounds(this Bounds bounds, Bounds target)
    {
        return bounds.Contains(target.min) && bounds.Contains(target.max);
    }

    public static Bounds OrthographicBounds()
    {
        var t = Cam.transform;
        var x = t.position.x;
        var y = t.position.y;
        var size = Cam.orthographicSize * 2;
        var width = size * (float)Screen.width / Screen.height;
        var height = size;

        return new Bounds(new Vector3(x, y, 0), new Vector3(width, height, 0));
    }
}