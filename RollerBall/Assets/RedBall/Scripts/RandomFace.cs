﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DragonBones;

public class RandomFace : MonoBehaviour
{
    [SerializeField] private float MinTimeChange;
    [SerializeField] private float MaxTimeChange;
    [SerializeField] private FaceBall AllFace;
    [SerializeField] private UnityArmatureComponent Anim;

    private void Start()
    {
        StopCoroutine("PlayAnima");
        StartCoroutine("PlayAnima");
    }

    private IEnumerator PlayAnima()
    {
        Anim.animation.Play("BinhThuong");
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(MinTimeChange, MaxTimeChange));
            AllFace = (FaceBall)System.Enum.ToObject(typeof(FaceBall), Random.Range(0, 10));
            switch (AllFace)
            {
                case FaceBall.BinhThuong:
                    {
                        Anim.animation.Play("BinhThuong");
                        break;
                    }
                case FaceBall.CuoiGatGu:
                    {
                        Anim.animation.Play("CuoiGatGu");
                        break;
                    }
                case FaceBall.LiecMat:
                    {
                        Anim.animation.Play("LiecMat");
                        break;
                    }
                case FaceBall.CuoiHipMat:
                    {
                        Anim.animation.Play("CuoiHip");
                        break;
                    }
                case FaceBall.NgacNhien:
                    {
                        Anim.animation.Play("NgacNhien");
                        break;
                    }
                case FaceBall.MatNgu:
                    {
                        Anim.animation.Play("MatNgu");
                        break;
                    }
            }
        }
    }

}
