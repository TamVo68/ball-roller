﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class StageSelectCTRL : MonoBehaviour
{
    [SerializeField] private int StageID;
    [SerializeField] private Button SelectBTN;
    [SerializeField] private TextMeshProUGUI StageNum;

    bool CheckUnlockStage()
    {
        if (ConstantManager.Data.ConfigData.UnlockAllStage)
        {
            return true;
        }
        else
        {
            if (ConstantManager.Data.PlayeData.CurrentStage >= StageID + 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        
    }

    private void Start()
    {
        StageNum.text = (StageID + 1).ToString();
        SelectBTN.onClick.AddListener(() => 
        {
            EventManager.Event_SelectStage(StageID);
        });

        SelectBTN.interactable = CheckUnlockStage();
    }

    public void InputInfo(int _StageID)
    {
        StageID = _StageID;
    }
}
