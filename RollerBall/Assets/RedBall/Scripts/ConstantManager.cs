﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConstantManager
{
    public static RollerBallManager Game_Manager;
    public static DataManager Data;
}

public class Self
{
    public int CurrentStage = 1;
    public int Coin = 0;
    public int Life = 1;
}

[System.Serializable]
public class Config
{
    #region Control
    [Header("Movement Player-----------------------")]
    public float Rolling_Velocity;
    public float Air_Velocity;
    public float Max_Air_Velocity;
    public float Spin_Speed;
    public float JumpStrenght;
    public float TombStrenght;
    public float ImpactHigh;
    public float StunTime;
    public float SpringJumpStrenght;
    public float Gravity;
    #endregion

    #region GamePlay
    [Header("Game Play-----------------------------")]
    public float TimeProtect;
    public int PointPerCoin;
    public float Max_TerrianAngle;
    #endregion

    #region Option Playing
    [Header("Option Play-----------------------------")]
    public bool UnlockAllStage;
    #endregion
}

public enum PlayerState
{
    LIVE,
    STUN,
    WIN,
    DEAD
}

public enum MoveType
{
    STAND,
    UP_DOWN,
    PATROL,
    RIGHT_BACK,
    LEFT_BACK
}

public enum ActionType
{
    NONE,
    SHOT_DOWN,
    SHOT_UP,
    SHOT_RIGHT,
    SHOT_LEFT,
    LAUNCH_RIGHT,
    LAUNCH_LEFT
}

public enum TerrianType
{
    GROUND,
    OBJECT,
    AIR
}

public enum WallType
{
    NONE,
    SLOPE,
    WALL
}

public enum MachineType
{
    Door,
    DouDoor,
    LockDoor,
    Crusher,
    Elevator,
    Saw,
    Spin_Knife
}

public enum FaceBall
{
    LiecMat,
    BinhThuong,
    MatNgu,
    TucGian,
    Buon,
    NgacNhien,
    ChienThang,
    CuoiGatGu,
    CuoiGuong,
    CuoiHipMat
}


public enum DoorType
{
    UP,
    DOWN,
    LEFT,
    RIGHT
}

public enum SwitchType
{
    Button,
    Lever
}
