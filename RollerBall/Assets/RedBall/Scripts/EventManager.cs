﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class EventManager : MonoBehaviour
{
    #region Edit
    public static System.Action Event_Update_PlayerState;
    #endregion

    #region Playing
    public static System.Action Event_UpdateTakeStar;
    public static System.Action Event_UpdateCoin;
    public static System.Action Event_UpdateKey;
    public static System.Action Event_TakeStar;
    public static System.Action Event_TakeCoin;
    public static System.Action Event_TakeKey;
    public static System.Action<int> Event_SubHP;
    #endregion

    public static System.Action<int> Event_SelectStage;
    public static System.Action<Transform, TilemapRenderer,int, int, Transform> Event_StartGame;
    public static System.Action Event_GameOver;
    public static System.Action<bool> Event_WinLose;
}
